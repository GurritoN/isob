﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crypter
{
	public static class Crypter
	{
		public static class DES
        {
            private static ulong[] ToBinary(byte[] message)
            {
                if (message.Length % 8 != 0)
                {
                    byte[] buff = new byte[message.Length / 8 * 8 + 8];
                    message.CopyTo(buff, 0);
                    message = buff;
                }
                ulong[] binary = new ulong[message.Length / 8];
                for (int i = 0; i < message.Length / 8; i++)
                {
                    binary[i] = (ulong)message[i * 8 + 0] << 56 | (ulong)message[i * 8 + 1] << 48 | (ulong)message[i * 8 + 2] << 40 | (ulong)message[i * 8 + 3] << 32 |
                                (ulong)message[i * 8 + 4] << 24 | (ulong)message[i * 8 + 5] << 16 | (ulong)message[i * 8 + 6] << 8  | (ulong)message[i * 8 + 7] << 0;
                }
                return binary;
            }
            private static byte[] ToBytes(ulong[] binary)
            {
                byte[] message = new byte[binary.Length * 8];
                for (int i = 0; i < binary.Length; i++)
                {
                    message[8 * i + 0] = (byte)(binary[i] >> 56);
                    message[8 * i + 1] = (byte)(binary[i] >> 48);
                    message[8 * i + 2] = (byte)(binary[i] >> 40);
                    message[8 * i + 3] = (byte)(binary[i] >> 32);
                    message[8 * i + 4] = (byte)(binary[i] >> 24);
                    message[8 * i + 5] = (byte)(binary[i] >> 16);
                    message[8 * i + 6] = (byte)(binary[i] >> 8);
                    message[8 * i + 7] = (byte)(binary[i] >> 0);
                }
                return message;
            }
            private static byte[] IPIndexes = { 57, 49, 41, 33, 25, 17,  9,  1,
                                                 59, 51, 43, 35, 27, 19, 11,  3,
                                                 61, 53, 45, 37, 29, 21, 13,  5,
                                                 63, 55, 47, 39, 31, 23, 15,  7,
                                                 56, 48, 40, 32, 24, 16,  8,  0,
                                                 58, 50, 42, 34, 26, 18, 10,  2,
                                                 60, 52, 44, 36, 28, 20, 12,  4,
                                                 62, 54, 46, 38, 30, 22, 14,  6};
            private static ulong GetBit(ulong block, int index)
            {
                return (block >> index) % 2;
            }
            private static ulong Permutate(ulong block, bool invert, byte[] indexes)
            {
                ulong result = 0;
                for (int i = 0; i < indexes.Length; i++)
                {
                    ulong newBit;
                    if (!invert)
                        newBit = GetBit(block, indexes[i]);
                    else
                    {
                        int index = Array.IndexOf(indexes, (byte)i);
                        newBit = GetBit(block, index);
                    }
                    result = result | (newBit << i);
                }

                return result;
            }

            private static ulong[] SBox1 = { 14, 0, 4, 15, 13, 7, 1, 4, 2, 14, 15, 2, 11, 13, 8, 1, 3, 10, 10, 6, 6, 12, 12, 11, 5, 9, 9, 5, 0, 3, 7, 8, 4, 15, 1, 12, 14, 8, 8, 2, 13, 4, 6, 9, 2, 1, 11, 7, 15, 5, 12, 11, 9, 3, 7, 14, 3, 10, 10, 0, 5, 6, 0, 13 };
            private static ulong[] SBox2 = { 15, 3, 1, 13, 8, 4, 14, 7, 6, 15, 11, 2, 3, 8, 4, 14, 9, 12, 7, 0, 2, 1, 13, 10, 12, 6, 0, 9, 5, 11, 10, 5, 0, 13, 14, 8, 7, 10, 11, 1, 10, 3, 4, 15, 13, 4, 1, 2, 5, 11, 8, 6, 12, 7, 6, 12, 9, 0, 3, 5, 2, 14, 15, 9 };
            private static ulong[] SBox3 = { 10, 13, 0, 7, 9, 0, 14, 9, 6, 3, 3, 4, 15, 6, 5, 10, 1, 2, 13, 8, 12, 5, 7, 14, 11, 12, 4, 11, 2, 15, 8, 1, 13, 1, 6, 10, 4, 13, 9, 0, 8, 6, 15, 9, 3, 8, 0, 7, 11, 4, 1, 15, 2, 14, 12, 3, 5, 11, 10, 5, 14, 2, 7, 12 };
            private static ulong[] SBox4 = { 7, 13, 13, 8, 14, 11, 3, 5, 0, 6, 6, 15, 9, 0, 10, 3, 1, 4, 2, 7, 8, 2, 5, 12, 11, 1, 12, 10, 4, 14, 15, 9, 10, 3, 6, 15, 9, 0, 0, 6, 12, 10, 11, 1, 7, 13, 13, 8, 15, 9, 1, 4, 3, 5, 14, 11, 5, 12, 2, 7, 8, 2, 4, 14 };
            private static ulong[] SBox5 = { 2, 14, 12, 11, 4, 2, 1, 12, 7, 4, 10, 7, 11, 13, 6, 1, 8, 5, 5, 0, 3, 15, 15, 10, 13, 3, 0, 9, 14, 8, 9, 6, 4, 11, 2, 8, 1, 12, 11, 7, 10, 1, 13, 14, 7, 2, 8, 13, 15, 6, 9, 15, 12, 0, 5, 9, 6, 10, 3, 4, 0, 5, 14, 3 };
            private static ulong[] SBox6 = { 12, 10, 1, 15, 10, 4, 15, 2, 9, 7, 2, 12, 6, 9, 8, 5, 0, 6, 13, 1, 3, 13, 4, 14, 14, 0, 7, 11, 5, 3, 11, 8, 9, 4, 14, 3, 15, 2, 5, 12, 2, 9, 8, 5, 12, 15, 3, 10, 7, 11, 0, 14, 4, 1, 10, 7, 1, 6, 13, 0, 11, 8, 6, 13 };
            private static ulong[] SBox7 = { 4, 13, 11, 0, 2, 11, 14, 7, 15, 4, 0, 9, 8, 1, 13, 10, 3, 14, 12, 3, 9, 5, 7, 12, 5, 2, 10, 15, 6, 8, 1, 6, 1, 6, 4, 11, 11, 13, 13, 8, 12, 1, 3, 4, 7, 10, 14, 7, 10, 9, 15, 5, 6, 0, 8, 15, 0, 14, 5, 2, 9, 3, 2, 12 };
            private static ulong[] SBox8 = { 13, 1, 2, 15, 8, 13, 4, 8, 6, 10, 15, 3, 11, 7, 1, 4, 10, 12, 9, 5, 3, 5, 14, 11, 5, 0, 0, 14, 12, 9, 7, 2, 7, 2, 11, 1, 4, 14, 1, 7, 9, 4, 12, 10, 14, 8, 2, 13, 0, 15, 6, 12, 10, 9, 13, 0, 15, 3, 3, 5, 5, 6, 8, 11 };
            private static byte[] roundIndexes = { 7, 28, 21, 10, 26, 2, 19, 13, 23, 29, 5, 0, 18, 8, 24, 30, 22, 1, 14, 27, 6, 9, 17, 31, 15, 4, 20, 3, 11, 12, 25, 16 };

            private static ulong Extend(ulong block)
            {
                ulong result = 0;
                for (int i = 0; i < 8; i++)
                {
                    if (i == 0)
                        result |= GetBit(block, 31);
                    else
                        result |= GetBit(block, i * 4 - 1) << (i * 6);
                    result |= GetBit(block, i * 4    ) << (i * 6 + 1);
                    result |= GetBit(block, i * 4 + 1) << (i * 6 + 2);
                    result |= GetBit(block, i * 4 + 2) << (i * 6 + 3);
                    result |= GetBit(block, i * 4 + 3) << (i * 6 + 4);
                    if (i == 7)
                        result |= GetBit(block, 0) << 47;
                    else
                        result |= GetBit(block, i * 4 + 4) << (i * 6 + 5);
                }
                return result;
            }
            private static ulong Shrink(ulong block)
            {
                ulong result = 0;
                result |= SBox1[block << 58 >> 58];
                result |= SBox2[block << 52 >> 58] << 4;
                result |= SBox3[block << 46 >> 58] << 8;
                result |= SBox4[block << 40 >> 58] << 12;
                result |= SBox5[block << 34 >> 58] << 16;
                result |= SBox6[block << 28 >> 58] << 20;
                result |= SBox7[block << 22 >> 58] << 24;
                result |= SBox8[block << 16 >> 58] << 28;
                return result;
            }
            private static ulong Round(ulong block, ulong key, int iteration)
            {
                ulong L = block >> 32;
                ulong R = block << 32 >> 32;

                key = ShiftKey(key, iteration);

                ulong extendedR = Extend(R);
                extendedR = extendedR ^ key;
                ulong newR = Shrink(extendedR);
                newR = Permutate(newR, false, roundIndexes);

                ulong result = R << 32;
                result |= newR ^ L;
                return result;
            }

            private static byte[] keyIndexes = { 60, 52, 44, 36, 59, 51, 43, 35, 27, 19, 11, 3, 58, 50, 42, 34, 26, 18, 10, 2, 57, 49, 41, 33, 25, 17, 9, 1, 28, 20, 12, 4, 61, 53, 45, 37, 29, 21, 13, 5, 62, 54, 46, 38, 30, 22, 14, 6, 63, 55, 47, 39, 31, 23, 15, 7 };
            private static byte[] roundKeyIndexes = { 24, 27, 20, 6, 14, 10, 3, 22, 0, 17, 7, 12, 8, 23, 11, 5, 16, 26, 1, 9, 19, 25, 4, 15, 54, 43, 36, 29, 49, 40, 48, 30, 52, 44, 37, 33, 46, 35, 50, 41, 28, 53, 51, 55, 32, 45, 39, 42 };
            private static byte[] shifts = { 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };
            private static ulong ShiftKey(ulong key, int iteration)
            {
                ulong L = key >> 28;
                ulong R = key << 36 >> 28;
                L = L << shifts[iteration] | L >> (28 - shifts[iteration]);
                R = R << shifts[iteration] | R >> (28 - shifts[iteration]);
                key = L << 28 | R;
                key = Permutate(key, false, roundKeyIndexes);
                return key;
            }

            public static byte[] Encrypt(byte[] _message, byte[] _key)
            {
                ulong key = BitConverter.ToUInt64(_key, 0);
                ulong[] binary = ToBinary(_message);
                key = Permutate(key, false, keyIndexes);
                for (int j = 0; j < binary.Length; j++)
                {
                    binary[j] = Permutate(binary[j], false, IPIndexes);
                    for (int i = 0; i < 16; i++)
                    {
                        binary[j] = Round(binary[j], key, i);
                    }
                    binary[j] = (binary[j] << 32 >> 32) << 32 | (binary[j] >> 32);
                    binary[j] = Permutate(binary[j], true, IPIndexes);
                }
                return ToBytes(binary);
            }

            public static byte[] Decrypt(byte[] _message, byte[] _key)
            {
                ulong key = BitConverter.ToUInt64(_key, 0);
                ulong[] binary = ToBinary(_message);
                key = Permutate(key, false, keyIndexes);
                for (int j = 0; j < binary.Length; j++)
                {
                    binary[j] = Permutate(binary[j], false, IPIndexes);
                    for (int i = 15; i >= 0; i--)
                    {
                        binary[j] = Round(binary[j], key, i);
                    }
                    binary[j] = (binary[j] << 32 >> 32) << 32 | (binary[j] >> 32);
                    binary[j] = Permutate(binary[j], true, IPIndexes);
                }
                return ToBytes(binary);
            }
        }

        public static class STB3410131
        {
            private static byte[] HTable = new byte[]
            {
                0xB1, 0x94, 0xBA, 0xC8, 0x0A, 0x08, 0xF5, 0x3B, 0x36, 0x6D, 0x00, 0x8E, 0x58, 0x4A, 0x5D, 0xE4,
                0x85, 0x04, 0xFA, 0x9D, 0x1B, 0xB6, 0xC7, 0xAC, 0x25, 0x2E, 0x72, 0xC2, 0x02, 0xFD, 0xCE, 0x0D,
                0x5B, 0xE3, 0xD6, 0x12, 0x17, 0xB9, 0x61, 0x81, 0xFE, 0x67, 0x86, 0xAD, 0x71, 0x6B, 0x89, 0x0B,
                0x5C, 0xB0, 0xC0, 0xFF, 0x33, 0xC3, 0x56, 0xB8, 0x35, 0xC4, 0x05, 0xAE, 0xD8, 0xE0, 0x7F, 0x99,
                0xE1, 0x2B, 0xDC, 0x1A, 0xE2, 0x82, 0x57, 0xEC, 0x70, 0x3F, 0xCC, 0xF0, 0x95, 0xEE, 0x8D, 0xF1,
                0xC1, 0xAB, 0x76, 0x38, 0x9F, 0xE6, 0x78, 0xCA, 0xF7, 0xC6, 0xF8, 0x60, 0xD5, 0xBB, 0x9C, 0x4F,
                0xF3, 0x3C, 0x65, 0x7B, 0x63, 0x7C, 0x30, 0x6A, 0xDD, 0x4E, 0xA7, 0x79, 0x9E, 0xB2, 0x3D, 0x31,
                0x3E, 0x98, 0xB5, 0x6E, 0x27, 0xD3, 0xBC, 0xCF, 0x59, 0x1E, 0x18, 0x1F, 0x4C, 0x5A, 0xB7, 0x93,
                0xE9, 0xDE, 0xE7, 0x2C, 0x8F, 0x0C, 0x0F, 0xA6, 0x2D, 0xDB, 0x49, 0xF4, 0x6F, 0x73, 0x96, 0x47,
                0x06, 0x07, 0x53, 0x16, 0xED, 0x24, 0x7A, 0x37, 0x39, 0xCB, 0xA3, 0x83, 0x03, 0xA9, 0x8B, 0xF6,
                0x92, 0xBD, 0x9B, 0x1C, 0xE5, 0xD1, 0x41, 0x01, 0x54, 0x45, 0xFB, 0xC9, 0x5E, 0x4D, 0x0E, 0xF2,
                0x68, 0x20, 0x80, 0xAA, 0x22, 0x7D, 0x64, 0x2F, 0x26, 0x87, 0xF9, 0x34, 0x90, 0x40, 0x55, 0x11,
                0xBE, 0x32, 0x97, 0x13, 0x43, 0xFC, 0x9A, 0x48, 0xA0, 0x2A, 0x88, 0x5F, 0x19, 0x4B, 0x09, 0xA1,
                0x7E, 0xCD, 0xA4, 0xD0, 0x15, 0x44, 0xAF, 0x8C, 0xA5, 0x84, 0x50, 0xBF, 0x66, 0xD2, 0xE8, 0x8A,
                0xA2, 0xD7, 0x46, 0x52, 0x42, 0xA8, 0xDF, 0xB3, 0x69, 0x74, 0xC5, 0x51, 0xEB, 0x23, 0x29, 0x21,
                0xD4, 0xEF, 0xD9, 0xB4, 0x3A, 0x62, 0x28, 0x75, 0x91, 0x14, 0x10, 0xEA, 0x77, 0x6C, 0xDA, 0x1D
            };
            private static byte H(byte u)
            {
                return HTable[u];
            }
            private static uint RotHi(uint u, int r)
            {
                uint Hi = u >> (32 - r);
                return u << r | Hi;
            }
            private static uint G(uint u, int r)
            {
                byte u1 = (byte)(u >> 24);
                byte u2 = (byte)(u >> 16);
                byte u3 = (byte)(u >> 8);
                byte u4 = (byte)(u >> 0);
                u = (uint)(H(u1) << 24) | (uint)(H(u2) << 16) | (uint)(H(u3) << 8) | (uint)(H(u4) << 0);
                return RotHi(u, r);
            }

            public static uint[] EncryptBlock(uint[] block, uint[] key)
            {
                uint a, b, c, d, e;
                a = block[0];
                b = block[1];
                c = block[2];
                d = block[3];
                uint t;
                for (int i = 1; i <= 8; i++)
                {
                    b = b ^ G(a + key[(7 * i - 6) % 8], 5);
                    c = c ^ G(d + key[(7 * i - 5) % 8], 21);
                    a = a - G(b + key[(7 * i - 4) % 8], 13);
                    e = G(b + c + key[(7 * i - 3) % 8], 21) ^ (uint)i;
                    b = b + e;
                    c = c - e;
                    d = d + G(c + key[(7 * i - 2) % 8], 13);
                    b = b ^ G(a + key[(7 * i - 1) % 8], 21);
                    c = c ^ G(d + key[(7 * i - 0) % 8], 5);

                    t = a;
                    a = b;
                    b = t;

                    t = c;
                    c = d;
                    d = t;

                    t = b;
                    b = c;
                    c = t;
                }

                return new uint[4] { b, d, a, c };
            }
            public static uint[] DecryptBlock(uint[] block, uint[] key)
            {
                uint a, b, c, d, e;
                a = block[0];
                b = block[1];
                c = block[2];
                d = block[3];
                uint t;
                for (int i = 8; i >= 1; i--)
                {
                    b = b ^ G(a + key[(7 * i - 0) % 8], 5);
                    c = c ^ G(d + key[(7 * i - 1) % 8], 21);
                    a = a - G(b + key[(7 * i - 2) % 8], 13);
                    e = G(b + c + key[(7 * i - 3) % 8], 21) ^ (uint)i;
                    b = b + e;
                    c = c - e;
                    d = d + G(c + key[(7 * i - 4) % 8], 13);
                    b = b ^ G(a + key[(7 * i - 5) % 8], 21);
                    c = c ^ G(d + key[(7 * i - 6) % 8], 5);

                    t = a;
                    a = b;
                    b = t;

                    t = c;
                    c = d;
                    d = t;

                    t = a;
                    a = d;
                    d = t;
                }

                return new uint[4] { c, a, d, b };
            }

            private static uint[] ToUIntArray(byte[] arr)
            {
                if (arr.Length % 16 != 0)
                {
                    byte[] buff = new byte[arr.Length / 16 * 16 + 16];
                    arr.CopyTo(buff, 0);
                    arr = buff;
                }
                uint[] uints = new uint[arr.Length / 4];
                for (int i = 0; i < arr.Length / 4; i++)
                {
                    uints[i] = (uint)arr[i * 4 + 0] << 24 | (uint)arr[i * 4 + 1] << 16 | (uint)arr[i * 4 + 2] << 8 | (uint)arr[i * 4 + 3] << 0;
                }
                return uints;
            }
            private static byte[] ToByteArray(uint[] arr)
            {
                byte[] bytes = new byte[arr.Length * 4];
                for (int i = 0; i < arr.Length; i++)
                {
                    bytes[4 * i + 0] = (byte)(arr[i] >> 24);
                    bytes[4 * i + 1] = (byte)(arr[i] >> 16);
                    bytes[4 * i + 2] = (byte)(arr[i] >> 8);
                    bytes[4 * i + 3] = (byte)(arr[i] >> 0);
                }
                return bytes;
            }

            public static byte[] GetR(uint[] arr, int count)
            {
                List<byte> r = new List<byte>();
                for (int i = 0; i < count; i++)
                {
                    int pos = i / 4;
                    r.Add((byte)(arr[arr.Length - 1 - pos] >> ((i % 4) * 8)));
                }
                return r.ToArray();
            }

            public static uint[] SetR(uint[] arr, byte[] r, int count)
            {
                for (int i = 0; i < count; i++)
                {
                    int pos = i / 4;
                    arr[arr.Length - 1 - pos] |= (uint)r[i] << ((i % 4) * 8);
                }
                return arr;
            }

            public static byte[] Plain(byte[] message, byte[] key, bool encrypt)
            {
                uint[] uintMessage = ToUIntArray(message);
                uint[] uintKey = ToUIntArray(key);
                int unusedBytes = uintMessage.Length * 4 - message.Length;
                byte[] r = new byte[0];
                uint[] rblock = new uint[4];
                for (int i = 0; i < uintMessage.Length / 4; i++)
                {
                    uint[] block;
                    block = new uint[4] { uintMessage[i * 4 + 0], uintMessage[i * 4 + 1], uintMessage[i * 4 + 2], uintMessage[i * 4 + 3] };
                    if (i == uintMessage.Length / 4 - 1)
                        block = SetR(block, r, unusedBytes);
                    if (encrypt)
                        block = EncryptBlock(block, uintKey);
                    else
                        block = DecryptBlock(block, uintKey);
                    if (i == uintMessage.Length / 4 - 2)
                    {
                        r = GetR(block, unusedBytes);
                        rblock[0] = block[0];
                        rblock[1] = block[1];
                        rblock[2] = block[2];
                        rblock[3] = block[3];
                    }
                    else if (i == uintMessage.Length / 4 - 1)
                    {
                        uintMessage[i * 4 - 4] = block[0];
                        uintMessage[i * 4 - 3] = block[1];
                        uintMessage[i * 4 - 2] = block[2];
                        uintMessage[i * 4 - 1] = block[3];
                        uintMessage[i * 4 + 0] = rblock[0];
                        uintMessage[i * 4 + 1] = rblock[1];
                        uintMessage[i * 4 + 2] = rblock[2];
                        uintMessage[i * 4 + 3] = rblock[3];
                    }
                    else
                    {
                        uintMessage[i * 4 + 0] = block[0];
                        uintMessage[i * 4 + 1] = block[1];
                        uintMessage[i * 4 + 2] = block[2];
                        uintMessage[i * 4 + 3] = block[3];
                    }
                }
                var ans = ToByteArray(uintMessage);
                return ans.Take(ans.Length - unusedBytes).ToArray();
            }

            public static byte[] Chain(byte[] message, byte[] key, byte[] S, bool encrypt)
            {
                uint[] uintMessage = ToUIntArray(message);
                uint[] uintKey = ToUIntArray(key);
                uint[] uintS = ToUIntArray(S);
                uint[] Y = uintS;
                int unusedBytes = uintMessage.Length * 4 - message.Length;
                uint[] uintAns = new uint[uintMessage.Length];
                byte[] r = new byte[0];
                for (int i = 0; i < uintMessage.Length / 4; i++)
                {
                    uint[] block;

                    if (encrypt)
                    {
                        if (i == 0)
                            block = new uint[4] { uintMessage[i * 4 + 0] ^ Y[0], uintMessage[i * 4 + 1] ^ Y[1], uintMessage[i * 4 + 2] ^ Y[2], uintMessage[i * 4 + 3] ^ Y[3] };
                        else if (i == uintMessage.Length / 4 - 1)
                        {
                            block = new uint[4] { uintMessage[i * 4 + 0] ^ uintAns[i * 4 + 0], uintMessage[i * 4 + 1] ^ uintAns[i * 4 + 1], uintMessage[i * 4 + 2] ^ uintAns[i * 4 + 2], uintMessage[i * 4 + 3] ^ uintAns[i * 4 + 3] };
                            block = SetR(block, r, unusedBytes);
                        }
                        else
                            block = new uint[4] { uintMessage[i * 4 + 0] ^ uintAns[i * 4 - 4], uintMessage[i * 4 + 1] ^ uintAns[i * 4 - 3], uintMessage[i * 4 + 2] ^ uintAns[i * 4 - 2], uintMessage[i * 4 + 3] ^ uintAns[i * 4 - 1] };
                    }
                    else
                    {
                        block = new uint[4] { uintMessage[i * 4 + 0], uintMessage[i * 4 + 1], uintMessage[i * 4 + 2], uintMessage[i * 4 + 3] };
                        if (i == uintMessage.Length / 4 - 1)
                            block = SetR(block, r, unusedBytes);
                    }
                    if (encrypt)
                        block = EncryptBlock(block, uintKey);
                    else
                        block = DecryptBlock(block, uintKey);
                    if (encrypt)
                    {
                        if (i == uintMessage.Length / 4 - 2)
                        {
                            r = GetR(block, unusedBytes);
                            uintAns[i * 4 + 4] = block[0];
                            uintAns[i * 4 + 5] = block[1];
                            uintAns[i * 4 + 6] = block[2];
                            uintAns[i * 4 + 7] = block[3];
                        }
                        else if (i == uintMessage.Length / 4 - 1)
                        {
                            uintAns[i * 4 - 4] = block[0];
                            uintAns[i * 4 - 3] = block[1];
                            uintAns[i * 4 - 2] = block[2];
                            uintAns[i * 4 - 1] = block[3];
                        }
                        else
                        {
                            uintAns[i * 4 + 0] = block[0];
                            uintAns[i * 4 + 1] = block[1];
                            uintAns[i * 4 + 2] = block[2];
                            uintAns[i * 4 + 3] = block[3];
                        }
                    }
                    else
                    {
                        if (i == 0)
                        {
                            uintAns[i * 4 + 0] = block[0] ^ Y[0];
                            uintAns[i * 4 + 1] = block[1] ^ Y[1];
                            uintAns[i * 4 + 2] = block[2] ^ Y[2];
                            uintAns[i * 4 + 3] = block[3] ^ Y[3];
                        }
                        else if (i == uintMessage.Length / 4 - 2)
                        {
                            uintAns[i * 4 + 4] = block[0] ^ uintMessage[i * 4 + 4];
                            uintAns[i * 4 + 5] = block[1] ^ uintMessage[i * 4 + 5];
                            uintAns[i * 4 + 6] = block[2] ^ uintMessage[i * 4 + 6];
                            uintAns[i * 4 + 7] = block[3] ^ uintMessage[i * 4 + 7];
                            r = GetR(new uint[4] { uintAns[i * 4 + 4], uintAns[i * 4 + 5], uintAns[i * 4 + 6] , uintAns[i * 4 + 7] }, unusedBytes);
                        }
                        else if (i == uintMessage.Length / 4 - 1)
                        {
                            uintAns[i * 4 - 4] = block[0] ^ uintMessage[i * 4 - 8];
                            uintAns[i * 4 - 3] = block[1] ^ uintMessage[i * 4 - 7];
                            uintAns[i * 4 - 2] = block[2] ^ uintMessage[i * 4 - 6];
                            uintAns[i * 4 - 1] = block[3] ^ uintMessage[i * 4 - 5];
                        }
                        else
                        {
                            uintAns[i * 4 + 0] = block[0] ^ uintMessage[i * 4 - 4];
                            uintAns[i * 4 + 1] = block[1] ^ uintMessage[i * 4 - 3];
                            uintAns[i * 4 + 2] = block[2] ^ uintMessage[i * 4 - 2];
                            uintAns[i * 4 + 3] = block[3] ^ uintMessage[i * 4 - 1];
                        }
                    }
                }
                var ans = ToByteArray(uintAns);
                return ans.Take(ans.Length - unusedBytes).ToArray();
            }

            public static byte[] Gamma(byte[] message, byte[] key, byte[] S, bool encrypt)
            {
                uint[] uintMessage = ToUIntArray(message);
                uint[] uintKey = ToUIntArray(key);
                uint[] uintS = ToUIntArray(S);
                uint[] Y = uintS;
                int unusedBytes = uintMessage.Length * 4 - message.Length;
                for (int i = 0; i < uintMessage.Length / 4; i++)
                {
                    uint[] block;
                    uint[] rblock = new uint[4];

                    if (encrypt)
                        block = Y;
                    else
                    {
                        block = Y;
                        rblock = new uint[4] { uintMessage[i * 4 + 0], uintMessage[i * 4 + 1], uintMessage[i * 4 + 2], uintMessage[i * 4 + 3] };
                    }
                    block = EncryptBlock(block, uintKey);
                    uintMessage[i * 4 + 0] ^= block[0];
                    uintMessage[i * 4 + 1] ^= block[1];
                    uintMessage[i * 4 + 2] ^= block[2];
                    uintMessage[i * 4 + 3] ^= block[3];
                    if (encrypt)
                        Y = new uint[4] { uintMessage[i * 4 + 0], uintMessage[i * 4 + 1], uintMessage[i * 4 + 2], uintMessage[i * 4 + 3] };
                    else
                        Y = rblock;
                }
                var ans = ToByteArray(uintMessage);
                return ans.Take(ans.Length - unusedBytes).ToArray();
            }

            private static uint[] Increment128(uint[] value)
            {
                value[3]++;
                if (value[3] == 0)
                    value[2]++;
                if (value[2] == 0)
                    value[1]++;
                if (value[1] == 0)
                    value[0]++;
                return value;
            }

            public static byte[] Counter(byte[] message, byte[] key, byte[] S, bool encrypt)
            {
                uint[] uintMessage = ToUIntArray(message);
                uint[] uintKey = ToUIntArray(key);
                uint[] uintS = ToUIntArray(S);
                uint[] Y = uintS;
                int unusedBytes = uintMessage.Length * 4 - message.Length;
                for (uint i = 0; i < uintMessage.Length / 4; i++)
                {
                    uint[] block = Increment128(Y);

                    block = EncryptBlock(block, uintKey);
                    uintMessage[i * 4 + 0] ^= block[0];
                    uintMessage[i * 4 + 1] ^= block[1];
                    uintMessage[i * 4 + 2] ^= block[2];
                    uintMessage[i * 4 + 3] ^= block[3];
                }
                var ans = ToByteArray(uintMessage);
                return ans.Take(ans.Length - unusedBytes).ToArray();
            }
        }
	}

    public static class Helper
    {

        public static string GetString(this byte[] arr)
        {
            string str = "";
            for (int i = 0; i < arr.Length; i++)
            {
                str += Convert.ToString(arr[i], 16).PadLeft(2, '0') + " ";
            }
            return str;
        }

        public static byte[] DES(this byte[] message, byte[] key, bool encrypt)
        {
            if (encrypt)
                return Crypter.DES.Encrypt(message, key);
            else
                return Crypter.DES.Decrypt(message, key);
        }

        public static byte[] BelTPlain(this byte[] message, byte[] key, bool encrypt)
        {
            return Crypter.STB3410131.Plain(message, key, encrypt);
        }

        public static byte[] BelTChain(this byte[] message, byte[] key, byte[] S, bool encrypt)
        {
            return Crypter.STB3410131.Chain(message, key, S, encrypt);
        }

        public static byte[] BelTGamma(this byte[] message, byte[] key, byte[] S, bool encrypt)
        {
            return Crypter.STB3410131.Gamma(message, key, S, encrypt);
        }

        public static byte[] BelTCounter(this byte[] message, byte[] key, byte[] S, bool encrypt)
        {
            return Crypter.STB3410131.Counter(message, key, S, encrypt);
        }
    }
}
