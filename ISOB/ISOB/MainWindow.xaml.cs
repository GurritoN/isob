﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using Crypter;

namespace ISOB
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
        private Random r;

		public MainWindow()
		{
			InitializeComponent();
            r = new Random();
		}

        private byte[] Key1 = new byte[8];
        private byte[] Key2 = new byte[8];
        private byte[] Key3 = new byte[8];

        private string filePath;

        private void GenerateKeys(object sender, RoutedEventArgs e)
        {
            r.NextBytes(Key1);
            r.NextBytes(Key2);
            r.NextBytes(Key3);
            Keys.Text = $"Key1={Key1.GetString()}\n Key2={Key2.GetString()}\n Key3={Key3.GetString()}";
        }

        private void LoadKeys(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "Keys file (*.gck)|*.gck";

            if (openFileDialog.ShowDialog().Value)
            {
                using (var file = File.OpenRead(openFileDialog.FileName))
                {
                    file.Read(Key1, 0, 8);
                    file.Read(Key2, 0, 8);
                    file.Read(Key3, 0, 8);
                    Keys.Text = $"Key1={Key1.GetString()}\n Key2={Key2.GetString()}\n Key3={Key3.GetString()}";
                }
            }
        }

        private void SaveKeys(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "Keys file (*.gck)|*.gck";

            if (saveFileDialog.ShowDialog().Value)
            {
                using (var file = File.OpenWrite(saveFileDialog.FileName))
                {
                    file.Write(Key1, 0, 8);
                    file.Write(Key2, 0, 8);
                    file.Write(Key3, 0, 8);
                }
            }
        }

        private void LoadFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "Text files (*.txt)|*.txt";

            if (openFileDialog.ShowDialog().Value)
            {
                FileName.Text = openFileDialog.FileName;
            }
        }

        private void _2DESDecrypt(object sender, RoutedEventArgs e)
        {
            byte[] text = File.ReadAllBytes(FileName.Text);
            text = text.DES(Key2, false).DES(Key1, false);
            File.WriteAllBytes(FileName.Text, text);
        }

        private void _3DESDecrypt(object sender, RoutedEventArgs e)
        {
            byte[] text = File.ReadAllBytes(FileName.Text);
            text = text.DES(Key3, false).DES(Key2, false).DES(Key1, false);
            File.WriteAllBytes(FileName.Text, text);
        }

        private void _2DESEncrypt(object sender, RoutedEventArgs e)
        {
            byte[] text = File.ReadAllBytes(FileName.Text);
            text = text.DES(Key1, true).DES(Key2, true);
            File.WriteAllBytes(FileName.Text, text);
        }

        private void _3DESEncrypt(object sender, RoutedEventArgs e)
        {
            byte[] text = File.ReadAllBytes(FileName.Text);
            text = text.DES(Key1, true).DES(Key2, true).DES(Key3, true);
            File.WriteAllBytes(FileName.Text, text);
        }
    }
}
