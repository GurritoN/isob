﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using Crypter;

namespace ISOB
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
        private Random r;

		public MainWindow()
		{
			InitializeComponent();
            r = new Random();
		}

        private byte[] Key1 = new byte[32];
        private byte[] Key2 = new byte[16];

        private void GenerateKeys(object sender, RoutedEventArgs e)
        {
            r.NextBytes(Key1);
            r.NextBytes(Key2);
            Keys.Text = $"Key1={Key1.GetString()}\n Key2={Key2.GetString()}";
            SaveKey.IsEnabled = true;
            LoadFileBtn.IsEnabled = true;
        }

        private void LoadKeys(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "Keys file (*.gck)|*.gck";

            if (openFileDialog.ShowDialog().Value)
            {
                using (var file = File.OpenRead(openFileDialog.FileName))
                {
                    file.Read(Key1, 0, 32);
                    file.Read(Key2, 0, 16);
                    Keys.Text = $"Key1={Key1.GetString()}\n Key2={Key2.GetString()}";
                    SaveKey.IsEnabled = true;
                    LoadFileBtn.IsEnabled = true;
                }
            }
        }

        private void SaveKeys(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "Keys file (*.gck)|*.gck";

            if (saveFileDialog.ShowDialog().Value)
            {
                using (var file = File.OpenWrite(saveFileDialog.FileName))
                {
                    file.Write(Key1, 0, 32);
                    file.Write(Key2, 0, 16);
                }
            }
        }

        private void LoadFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "Text files (*.txt)|*.txt";

            if (openFileDialog.ShowDialog().Value)
            {
                FileName.Text = openFileDialog.FileName;
                PDec.IsEnabled = true;
                PEnc.IsEnabled = true;
                CDec.IsEnabled = true;
                CEnc.IsEnabled = true;
                GDec.IsEnabled = true;
                GEnc.IsEnabled = true;
                CntDec.IsEnabled = true;
                CntEnc.IsEnabled = true;
            }
        }

        private void PlainEncrypt(object sender, RoutedEventArgs e)
        {
            byte[] text = File.ReadAllBytes(FileName.Text);
            text = text.BelTPlain(Key1, true);
            File.WriteAllBytes(FileName.Text, text);
        }

        private void PlainDecrypt(object sender, RoutedEventArgs e)
        {
            byte[] text = File.ReadAllBytes(FileName.Text);
            text = text.BelTPlain(Key1, false);
            File.WriteAllBytes(FileName.Text, text);
        }

        private void ChainEncrypt(object sender, RoutedEventArgs e)
        {
            byte[] text = File.ReadAllBytes(FileName.Text);
            text = text.BelTChain(Key1, Key2, true);
            File.WriteAllBytes(FileName.Text, text);
        }

        private void ChainDecrypt(object sender, RoutedEventArgs e)
        {
            byte[] text = File.ReadAllBytes(FileName.Text);
            text = text.BelTChain(Key1, Key2, false);
            File.WriteAllBytes(FileName.Text, text);
        }

        private void GammaEncrypt(object sender, RoutedEventArgs e)
        {
            byte[] text = File.ReadAllBytes(FileName.Text);
            text = text.BelTGamma(Key1, Key2, true);
            File.WriteAllBytes(FileName.Text, text);
        }

        private void GammaDecrypt(object sender, RoutedEventArgs e)
        {
            byte[] text = File.ReadAllBytes(FileName.Text);
            text = text.BelTGamma(Key1, Key2, false);
            File.WriteAllBytes(FileName.Text, text);
        }

        private void CounterEncrypt(object sender, RoutedEventArgs e)
        {
            byte[] text = File.ReadAllBytes(FileName.Text);
            text = text.BelTCounter(Key1, Key2, true);
            File.WriteAllBytes(FileName.Text, text);
        }

        private void CounterDecrypt(object sender, RoutedEventArgs e)
        {
            byte[] text = File.ReadAllBytes(FileName.Text);
            text = text.BelTCounter(Key1, Key2, false);
            File.WriteAllBytes(FileName.Text, text);
        }
    }
}
